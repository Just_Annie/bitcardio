<?php

namespace App\Jobs;

use App\Mail\SimpleMail;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Log;
use Symfony\Component\HttpFoundation\ParameterBag;

abstract class SendEmailAbstractJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 25;

    /**
     * The maximum number of exceptions to allow before failing.
     *
     * @var int
     */
    public $maxExceptions = 3;

    public const QUEUE_EMAILS = 'emails';

    /**
     * @var ParameterBag
     */
    protected $context;

    abstract public function getSubject(): string;

    abstract public function getTemplate(): string;

    abstract public function getParams(): ParameterBag;

    abstract public function getEmail(): string;

    /**
     * Create a new job instance.
     *
     * @param ParameterBag $context
     */
    public function __construct(ParameterBag $context)
    {
        $this->context = $context;
        $this->queue = static::QUEUE_EMAILS;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if (empty($this->getSubject())
            || empty($this->getParams())
            || empty($this->getTemplate())
            || empty($this->getEmail())
        ) {
            Log::error(
                'Невозможно осуществить отправку email из-за пустых обязательных параметров',
                ['context' => [$this->getParams()->all()]]
            );
            return;
        }
        $email = $this->getEmail();
        $mail = new SimpleMail($this->getSubject(), $this->getParams(), $this->getTemplate());

        try {
            \Mail::to($email)->send($mail);
        } catch (\Throwable $exception) {
            Log::error('Email send error ' . $exception->getMessage());
        }
    }
}
