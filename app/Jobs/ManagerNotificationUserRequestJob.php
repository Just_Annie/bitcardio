<?php

namespace App\Jobs;

use App\Models\User;
use Symfony\Component\HttpFoundation\ParameterBag;

class ManagerNotificationUserRequestJob extends SendEmailAbstractJob
{

    public const TEMPLATE = 'emails.user-request-mail';

    /**
     * @return string
     */
    public function getSubject(): string
    {
        return $this->context->get('subject');
    }

    /**
     * @return string
     */
    public function getTemplate(): string
    {
        return static::TEMPLATE;
    }

    /**
     * @return ParameterBag
     */
    public function getParams(): ParameterBag
    {
        return $this->context;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->getManagerEmail();
    }

    public function handle()
    {

        parent::handle();
    }

    /**
     * @return string
     */
    private function getManagerEmail()
    {
        /** @var User $manager */
        $manager = User::where('type', User::TYPE_MANAGER)->first();
        return $manager->email;
    }
}
