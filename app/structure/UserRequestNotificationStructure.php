<?php

namespace App\structure;

use Symfony\Component\HttpFoundation\ParameterBag;

class UserRequestNotificationStructure extends ParameterBag
{
    public const F_ID = 'id';
    public const F_SUBJECT = 'subject';
    public const F_MESSAGE = 'message';
    public const F_NAME = 'name';
    public const F_EMAIL = 'email';
    public const F_LINK = 'link';
    public const F_CREATED_AT = 'created_at';
}