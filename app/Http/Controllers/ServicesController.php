<?php

namespace App\Http\Controllers;

use Illuminate\View\View;

/**
 * Class ServicesController
 * @package App\Http\Controllers
 */
class ServicesController extends Controller
{
    /**
     * @return View
     */
    public function index() : View
    {
        return view('services');
    }
}
