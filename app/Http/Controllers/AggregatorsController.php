<?php

namespace App\Http\Controllers;

use Illuminate\View\View;

/**
 * Class AggregatorsController
 * @package App\Http\Controllers
 */
class AggregatorsController extends Controller
{
    /**
     * @return View
     */
    public function index() : View
    {
        return view('aggregators');
    }
}
