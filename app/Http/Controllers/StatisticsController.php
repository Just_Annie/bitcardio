<?php

namespace App\Http\Controllers;

use Illuminate\View\View;

/**
 * Class StatisticsController
 * @package App\Http\Controllers
 */
class StatisticsController extends Controller
{
    /**
     * @return View
     */
    public function index() : View
    {
        return view('statistics');
    }
}
