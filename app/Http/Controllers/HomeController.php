<?php

namespace App\Http\Controllers;

use App\Services\UserRequestService;
use Auth;
use Illuminate\Contracts\Support\Renderable;

class HomeController extends Controller
{
    /**
     * @var UserRequestService
     */
    protected $userRequestService;

    /**
     * HomeController constructor.
     * @param UserRequestService $userRequestService
     */
    public function __construct(UserRequestService $userRequestService)
    {
        $this->userRequestService = $userRequestService;
    }

    /**
     * Show the application dashboard.
     *
     * @return Renderable
     */
    public function index()
    {
        $list = $this->userRequestService->getRequestsList();
        if ($list === null) {
            $sendRequestPermission = !$this->userRequestService->checkTodaySendingRequestByUserId(Auth::id());
        }
        return view('home', [
            'sendRequestPermission' => $sendRequestPermission ?? null,
            'list' => $list ?? null,
        ]);
    }
}
