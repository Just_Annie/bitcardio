<?php

namespace App\Http\Controllers;

use Illuminate\View\View;

/**
 * Class MonitoringController
 * @package App\Http\Controllers
 */
class MonitoringController extends Controller
{
    /**
     * @return View
     */
    public function index() : View
    {
        return view('monitoring');
    }
}
