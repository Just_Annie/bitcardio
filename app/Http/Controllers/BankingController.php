<?php

namespace App\Http\Controllers;

use Illuminate\View\View;

/**
 * Class BankingController
 * @package App\Http\Controllers
 */
class BankingController extends Controller
{
    /**
     * @return View
     */
    public function index() : View
    {
        return view('banking');
    }
}
