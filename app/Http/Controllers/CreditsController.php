<?php

namespace App\Http\Controllers;

use Illuminate\View\View;

/**
 * Class CreditsController
 * @package App\Http\Controllers
 */
class CreditsController extends Controller
{
    /**
     * @return View
     */
    public function index() : View
    {
        return view('credits');
    }
}
