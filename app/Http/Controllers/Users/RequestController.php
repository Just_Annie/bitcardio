<?php

namespace App\Http\Controllers;

use App\Exceptions\UserRequestResolutionException;
use App\Http\Requests\UserHomeFormRequest;
use App\Services\Crm\CrmInterface;
use App\Services\FileService;
use App\Services\NotificationsService;
use App\Services\UserRequestService;
use Auth;
use Exception;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;
use Log;

class RequestController extends Controller
{
    /**
     * @var UserRequestService
     */
    protected $userRequestService;

    /**
     * @var FileService
     */
    protected $fileService;

    /**
     * @var NotificationsService
     */
    protected $notificationsService;

    /**
     * @var CrmInterface
     */
    protected $crmClient;

    /**
     * RequestController constructor.
     * @param UserRequestService $userRequestService
     * @param FileService $fileService
     * @param NotificationsService $notificationsService
     * @param CrmInterface $crm
     */
    public function __construct(
        UserRequestService $userRequestService,
        FileService $fileService,
        NotificationsService $notificationsService,
        CrmInterface $crm
    ) {
        $this->userRequestService = $userRequestService;
        $this->fileService = $fileService;
        $this->notificationsService = $notificationsService;
        $this->crmClient = $crm;
    }

    /**
     * @param UserHomeFormRequest $request
     * @return RedirectResponse|Redirector
     */
    public function sendRequest(UserHomeFormRequest $request)
    {
        $redirect = redirect()->route('client.home');
        $user = Auth::getUser();
        try {
            $file = $request->file('file');
            $file_path = $file ? $this->fileService->upload($file) : null;
            $subject = $request->get('subject');
            $message = $request->get('message');
            $userRequest = $this->userRequestService->createRequest(
                $user->id,
                compact('file_path', 'subject', 'message')
            );
            $this->crmClient->userRequestNotificationHandle($user->name);
        } catch (UserRequestResolutionException $exception) {
            Log::error($exception->getMessage(), $request->toArray());
            $redirect = redirect()->route('client.home')
                ->withErrors(['upload' => __('layout.today-already-send')]);
        } catch (Exception $exception) {
            Log::error($exception->getMessage(), $request->toArray());
            $redirect = redirect()->route('client.home')
                ->withErrors(['upload' => __('layout.send-request-error')]);
        }
        if (!empty($userRequest)) {
            $this->notificationsService->notifyManagerAboutUserRequest($user, $userRequest);
        }
        return $redirect->with('message','Спасибо за заявку!');
    }

    /**
     * @param int $id
     * @return RedirectResponse
     */
    public function markAsReadById(int $id)
    {
        try {
            $result = $this->userRequestService->markAsAnsweredById($id);
            if ($result === null) {
                return redirect()->back()->withErrors(['error' => __('layout.not-enough-permission')]);
            }
        } catch (Exception $exception) {
            return redirect()->back()->withErrors(['error' => $exception->getMessage()]);
        }

        return redirect()->back();
    }

}
