<?php

namespace App\Http\Controllers;

use Illuminate\View\View;

/**
 * Class ContactsController
 * @package App\Http\Controllers
 */
class ContactsController extends Controller
{
    /**
     * @return View
     */
    public function index() : View
    {
        return view('contacts');
    }
}
