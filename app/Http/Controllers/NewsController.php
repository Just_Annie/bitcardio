<?php

namespace App\Http\Controllers;

use Illuminate\View\View;

/**
 * Class NewsController
 * @package App\Http\Controllers
 */
class NewsController extends Controller
{
    /**
     * @return View
     */
    public function index() : View
    {
        return view('news');
    }
}
