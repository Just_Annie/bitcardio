<?php

namespace App\Http\Controllers;

use Illuminate\View\View;

/**
 * Class CardsController
 * @package App\Http\Controllers
 */
class CardsController extends Controller
{
    /**
     * @return View
     */
    public function index() : View
    {
        return view('cards');
    }
}
