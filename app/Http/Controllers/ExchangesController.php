<?php

namespace App\Http\Controllers;

use Illuminate\View\View;

/**
 * Class ExchangesController
 * @package App\Http\Controllers
 */
class ExchangesController extends Controller
{
    /**
     * @return View
     */
    public function index() : View
    {
        return view('exchanges');
    }
}
