<?php

namespace App\Http\Controllers;

use Illuminate\View\View;

/**
 * Class InsuranceController
 * @package App\Http\Controllers
 */
class InsuranceController extends Controller
{
    /**
     * @return View
     */
    public function index() : View
    {
        return view('insurance');
    }
}
