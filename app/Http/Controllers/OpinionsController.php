<?php

namespace App\Http\Controllers;

use Illuminate\View\View;

/**
 * Class OpinionsController
 * @package App\Http\Controllers
 */
class OpinionsController extends Controller
{
    /**
     * @return View
     */
    public function index() : View
    {
        return view('opinions');
    }
}
