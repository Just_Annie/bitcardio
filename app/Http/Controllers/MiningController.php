<?php

namespace App\Http\Controllers;

use Illuminate\View\View;

/**
 * Class MiningController
 * @package App\Http\Controllers
 */
class MiningController extends Controller
{
    /**
     * @return View
     */
    public function index() : View
    {
        return view('mining');
    }
}
