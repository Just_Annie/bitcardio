<?php

namespace App\Http\Controllers;

use Illuminate\View\View;

/**
 * Class DepositsController
 * @package App\Http\Controllers
 */
class DepositsController extends Controller
{
    /**
     * @return View
     */
    public function index() : View
    {
        return view('deposits');
    }
}
