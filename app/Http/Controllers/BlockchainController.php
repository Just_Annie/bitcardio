<?php

namespace App\Http\Controllers;

use Illuminate\View\View;

/**
 * Class BlockchainController
 * @package App\Http\Controllers
 */
class BlockchainController extends Controller
{
    /**
     * @return View
     */
    public function index() : View
    {
        return view('blockchain');
    }
}
