<?php

namespace App\Http\Controllers;

use Illuminate\View\View;

/**
 * Class TradingController
 * @package App\Http\Controllers
 */
class TradingController extends Controller
{
    /**
     * @return View
     */
    public function index() : View
    {
        return view('trading');
    }
}
