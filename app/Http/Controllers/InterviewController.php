<?php

namespace App\Http\Controllers;

use Illuminate\View\View;

/**
 * Class InterviewController
 * @package App\Http\Controllers
 */
class InterviewController extends Controller
{
    /**
     * @return View
     */
    public function index() : View
    {
        return view('interview');
    }
}
