<?php

namespace App\Http\Controllers;

use Illuminate\View\View;

/**
 * Class TodayController
 * @package App\Http\Controllers
 */
class TodayController extends Controller
{
    /**
     * @return View
     */
    public function index() : View
    {
        return view('today');
    }
}
