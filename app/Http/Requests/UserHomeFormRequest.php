<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserHomeFormRequest extends FormRequest
{
    public function authorize() : bool
    {
        return true;
    }

    public function rules()
    {
        return [
          'subject' => 'required|max:100',
          'message' => 'required|max:1000',
          'file' => 'sometimes|file',
        ];
    }

    protected function prepareForValidation()
    {
        $this->merge([
            'subject' => strip_tags($this->request->get('subject')),
            'message' => strip_tags($this->request->get('message')),
        ]);
    }
}