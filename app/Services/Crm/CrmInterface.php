<?php

namespace App\Services\Crm;

interface CrmInterface
{
    public function userRequestNotificationHandle(string $name);
}