<?php

namespace App\Services;

use App\Jobs\ManagerNotificationUserRequestJob;
use App\Models\User;
use App\Models\UserRequest;
use App\structure\UserRequestNotificationStructure;

class NotificationsService
{
    /**
     * @param User $user
     * @param UserRequest $userRequest
     */
    public function notifyManagerAboutUserRequest(User $user, UserRequest $userRequest)
    {
        $params = $this->prepareParams($user, $userRequest);
        ManagerNotificationUserRequestJob::dispatch($params);
    }

    /**
     * @param User $user
     * @param UserRequest $userRequest
     * @return UserRequestNotificationStructure
     */
    private function prepareParams(User $user, UserRequest $userRequest)
    {
        $context = new UserRequestNotificationStructure();
        $context->set(UserRequestNotificationStructure::F_ID, $user->id);
        $context->set(UserRequestNotificationStructure::F_SUBJECT, $userRequest->subject);
        $context->set(UserRequestNotificationStructure::F_MESSAGE, $userRequest->message);
        $context->set(UserRequestNotificationStructure::F_NAME, $user->name ?? 'Unknown');
        $context->set(UserRequestNotificationStructure::F_EMAIL, $user->email);
        $context->set(UserRequestNotificationStructure::F_LINK, $userRequest->file_path);
        $context->set(UserRequestNotificationStructure::F_CREATED_AT, $userRequest->created_at->format('d-m-Y'));
        return $context;
    }
}