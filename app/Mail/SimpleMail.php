<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Symfony\Component\HttpFoundation\ParameterBag;

class SimpleMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * @var ParameterBag
     */
    public $params;
    /**
     * @var string
     */
    public $template;
    /**
     * @var string
     */
    public $subject;

    /**
     * Create a new message instance.
     *
     * @param string $subject
     * @param ParameterBag $params
     * @param string $template
     */
    public function __construct(string $subject, ParameterBag $params, string $template)
    {
        $this->params   = $params;
        $this->subject  = $subject;
        $this->template = $template;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        if ($this->params === null || $this->subject === null) {
            return null;
        }

        return $this->view($this->template, [
            'params'  => $this->params,
            'subject' => $this->subject,
        ]);
    }
}
