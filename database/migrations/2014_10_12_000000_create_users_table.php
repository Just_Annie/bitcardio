<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \DB::connection()->getPdo()->exec(<<<'SQL'
            CREATE TABLE users (
                id SERIAL NOT NULL  PRIMARY KEY,
                name TEXT DEFAULT NULL,
                email TEXT DEFAULT NULL UNIQUE,
                password TEXT DEFAULT NULL,
                type TEXT DEFAULT 'user',
                remember_token TEXT DEFAULT NULL,
                created_at TIMESTAMP(0) WITH TIME ZONE NOT NULL DEFAULT now(),
                updated_at TIMESTAMP(0) WITH TIME ZONE,
                deleted_at TIMESTAMP(0) WITH TIME ZONE
            );
SQL
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
