## Install application

### Copy config
 ``cp .env.example .env``
 
### Create common network
``docker network create network``

### Start application
``docker-compose up -d``

### Install dependencies
``docker-compose exec app composer install``
``docker-compose exec app php artisan storage:link``

### Generate application key
``docker-compose exec app php artisan key:generate``

### Install frontend dependencies
``docker-compose exec node yarn install``

### Laravel IDE Helpers
``docker-compose exec app php artisan ide-helper:generate``
``docker-compose exec app php artisan ide-helper:meta``

### Worker
``docker-compose exec app php artisan queue:work``

### Open app
``http://localhost:80``

### Open rabbit
``http://localhost:15672``

###AmoCrm integration
``update env with your credentials``
``
CRM_AMO_SUBDOMAIN=
CRM_AMO_CLIENT_ID=
CRM_AMO_CLIENT_SECRET=
CRM_AMO_GRANT_TYPE=authorization_code
CRM_AMO_CODE=
CRM_AMO_REDIRECT_URI=
``

#### Default manager
``
email=test@domain.com
password=password
``