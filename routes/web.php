<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('welcome');

Auth::routes();

Route::get('/news', 'NewsController@index')->name('news');
Route::get('/interview', 'InterviewController@index')->name('interview');
Route::get('/opinions', 'OpinionsController@index')->name('opinions');
Route::get('/today', 'TodayController@index')->name('today');

Route::get('/banking', 'BankingController@index')->name('banking');
Route::get('/insurance', 'InsuranceController@index')->name('insurance');
Route::get('/credits', 'CreditsController@index')->name('credits');
Route::get('/deposits', 'DepositsController@index')->name('deposits');
Route::get('/cards', 'CardsController@index')->name('cards');

Route::get('/monitoring', 'MonitoringController@index')->name('monitoring');
Route::get('/blockchain', 'BlockchainController@index')->name('blockchain');
Route::get('/mining', 'MiningController@index')->name('mining');
Route::get('/trading', 'TradingController@index')->name('trading');
Route::get('/statistics', 'StatisticsController@index')->name('statistics');

Route::get('/services', 'ServicesController@index')->name('services');
Route::get('/exchanges', 'ExchangesController@index')->name('exchanges');
Route::get('/aggregators', 'AggregatorsController@index')->name('aggregators');
Route::get('/contacts', 'ContactsController@index')->name('contacts');



Route::group(['middleware' => ['auth'], 'as' => 'client.', 'prefix' => '/client'], function () {
    Route::get('/home', 'HomeController@index')->name('home');
    Route::post('/send-request', 'RequestController@sendRequest')->name('send.request');
    Route::post('/mark-as-read/{id}', 'RequestController@markAsReadById')->name('mark.as.read');
});

