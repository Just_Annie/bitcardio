<?php

namespace Tests\Unit;

use App\Models\User;
use App\Services\UserRequestService;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class UserRequestsServiceTest extends TestCase
{
use DatabaseTransactions;

    public const REQUEST_DATA = [
        'subject' => 'subject',
        'message' => 'message',
    ];
    /**
     * @var User
     */
    private $user;

    /**
     * @var User
     */
    private $manager;

    /**
     * @var UserRequestService
     */
    protected $requestService;

    /**
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->user = factory(User::class)->create();
        $this->manager = factory(User::class)->state('manager')->create();
        $this->requestService = $this->app->make(UserRequestService::class);
    }

    /**
     * @throws \App\Exceptions\UserRequestResolutionException
     */
    public function testSendRequest()
    {
        $this->requestService->createRequest($this->user->id, self::REQUEST_DATA);
        $this->assertDatabaseHas('users_requests', [
            'user_id' => $this->user->id,
            'subject' => 'subject',
            'message' => 'message',
        ]);
    }

    /**
     * @throws \App\Exceptions\UserRequestResolutionException
     */
    public function testGetRequestsList()
    {
        $this->be($this->manager);
        $this->requestService->createRequest($this->user->id, self::REQUEST_DATA);
        $list = $this->requestService->getRequestsList();
        $this->assertNotCount(0,$list->items());
    }

    /**
     * @throws \App\Exceptions\UserRequestResolutionException
     */
    public function testMarkAsRead()
    {
        $this->be($this->manager);
        $request = $this->requestService->createRequest($this->user->id, self::REQUEST_DATA);
        $this->requestService->markAsAnsweredById($request->id);
        $this->assertDatabaseHas('users_requests', [
            'id' => $request->id,
            'is_answered' => true,
        ]);
    }
}
