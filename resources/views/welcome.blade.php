@extends('layouts.app')

@section('content')

<section class="ne-bann-ers">
    <div class="row__container currency-logo-wrapper">
        <a href="https://currency.com/trading/signup?c=edzuuna6&pid=referral"
           target="_blank">
            <img src="https://bitcardio.com/wp-content/uploads/2020/04/2857Google728x90var2RU_9b7df3bf.png"
                 alt=""
                 class="wp-image-613"
            >
        </a>
    </div>
</section>
<hr>
<div class="row__container">

</div>
<section class="news">
    <div class="visualMarketBlock">
        <div class="row__container">
            <h1>Новости</h1>

            <div class="grid line_1_1_1 home-page-news">
                <div>
                    <article class="newsCell" style="position: relative">
                        <a href="https://currency.com/ru/microsoft-kupila-legendarnogo-sozdatelya-videoigr-bethesda-za-7-5-mlrd"
                           class="newsCell__link">
                            <div class="imageWrap">
                                <div class="imageCell">
                                    <img src="https://img.currency.com/imgs/articles/704xx/Fallout4Car.jpg"
                                         alt="Microsoft купила легендарного создателя видеоигр Bethesda за $7,5 млрд"
                                         title="Microsoft купила легендарного создателя видеоигр Bethesda за $7,5 млрд"
                                         class="image" loading="lazy">
                                </div>
                            </div>
                            <div class="newsCell__rubric">Новости рынков</div>
                            <div class="newsCell__title medium">Microsoft купила легендарного создателя видеоигр
                                Bethesda за $7,5 млрд
                            </div>
                            <div class="details">
                                <div>Yana Berman • <span data-timeago="1600710600">16 minutes ago</span></div>
                            </div>
                        </a>
                    </article>


                    <article class="newsCell groupContainer">
                        <div class="newsCell__rubric">Аналитика</div>
                        <a href="https://currency.com/ru/tekhnicheskij-analiz-eth-usd-na-21-27-sentyabrya"
                           class="newsCell__link">
                            <header class="newsCell__title medium">Технический анализ ETH/USD на 21-27 сентября
                            </header>
                        </a>
                        <div class="details">
                            <div>Rakesh Upadhyay • <span data-timeago="1600709580">33 minutes ago</span></div>
                        </div>
                    </article>


                    <article class="newsCell groupContainer">
                        <div class="newsCell__rubric">Аналитика</div>
                        <a href="https://currency.com/ru/btc-usd-prognoz-21-27-sentjabrja" class="newsCell__link">
                            <header class="newsCell__title medium">Прогноз курса BTC/USD на неделю с 21 по 27
                                сентября
                            </header>
                        </a>
                        <div class="details">
                            <div>Mikhail Karkhalev • <span data-timeago="1600688040">7 hours ago</span></div>
                        </div>
                    </article>


                    <article class="newsCell groupContainer">
                        <div class="newsCell__rubric">Аналитика</div>
                        <a href="https://currency.com/ru/tekhnicheskij-analiz-btc-usd-na-21-27-sentyabrya"
                           class="newsCell__link">
                            <header class="newsCell__title medium">Технический анализ BTC/USD на 21-27 сентября
                            </header>
                        </a>
                        <div class="details">
                            <div>Rakesh Upadhyay • <span data-timeago="1600682940">8 hours ago</span></div>
                        </div>
                    </article>


                    <article class="newsCell groupContainer">
                        <div class="newsCell__rubric">Новости рынков</div>
                        <a href="https://currency.com/ru/bytedance-ocenila-tiktok-v-60-mlrd" class="newsCell__link">
                            <header class="newsCell__title medium">ByteDance оценила TikTok в $60 млрд</header>
                        </a>
                        <div class="details">
                            <div>Yana Berman • <span data-timeago="1600680720">9 hours ago</span></div>
                        </div>
                    </article>


                </div>
                <div>
                    <article class="newsCell" style="position: relative">
                        <a href="https://currency.com/ru/opros-10-rossijskih-vkladchikov-hoteli-by-investirovat-v-kriptovalyuty"
                           class="newsCell__link">
                            <div class="imageWrap">
                                <div class="imageCell">
                                    <img src="https://img.currency.com/imgs/articles/704xx/man-3126802_1920_2.jpg"
                                         alt="Опрос: 10% российских вкладчиков хотели бы инвестировать в криптовалюты "
                                         title="Опрос: 10% российских вкладчиков хотели бы инвестировать в криптовалюты "
                                         class="image" loading="lazy">
                                </div>
                            </div>
                            <div class="newsCell__rubric">Новости рынков</div>
                            <div class="newsCell__title medium">Опрос: 10% российских вкладчиков хотели бы
                                инвестировать в криптовалюты
                            </div>
                            <div class="details">
                                <div>Yana Berman • <span data-timeago="1600679700">9 hours ago</span></div>
                            </div>
                        </a>
                    </article>


                    <article class="newsCell groupContainer">
                        <div class="newsCell__rubric">Новости рынков</div>
                        <a href="https://currency.com/ru/narodnyj-bank-kitaya-sohranil-osnovnuyu-stavku-bez-izmenenij"
                           class="newsCell__link">
                            <header class="newsCell__title medium">Народный банк Китая сохранил основную ставку без
                                изменений
                            </header>
                        </a>
                        <div class="details">
                            <div>Yana Berman • <span data-timeago="1600678680">9 hours ago</span></div>
                        </div>
                    </article>


                    <article class="newsCell groupContainer">
                        <div class="newsCell__rubric">Аналитика</div>
                        <a href="https://currency.com/ru/analiz-akcij-gazprom-prognoz-21-po-27-sentyabrya"
                           class="newsCell__link">
                            <header class="newsCell__title medium">Анализ акций Газпром: прогноз на неделю с 21 по
                                27 сентября
                            </header>
                        </a>
                        <div class="details">
                            <div>Стелла Киракосян • <span data-timeago="1600505640">11:54, 19 September 2020</span>
                            </div>
                        </div>
                    </article>


                    <article class="newsCell groupContainer">
                        <div class="newsCell__rubric">Аналитика</div>
                        <a href="https://currency.com/ru/analiz-akcij-sberbanka-prognoz-21-27-sentyabrya"
                           class="newsCell__link">
                            <header class="newsCell__title medium">Анализ акций Сбербанка: прогноз на неделю с 21 по
                                27 сентября
                            </header>
                        </a>
                        <div class="details">
                            <div>Стелла Киракосян • <span data-timeago="1600498920">10:02, 19 September 2020</span>
                            </div>
                        </div>
                    </article>


                    <article class="newsCell groupContainer">
                        <div class="newsCell__rubric">Новости рынков</div>
                        <a href="https://currency.com/ru/v-ssha-zapretyat-skachivat-tiktok-i-wechat"
                           class="newsCell__link">
                            <header class="newsCell__title medium">В США запретят скачивать TikTok и WeChat</header>
                        </a>
                        <div class="details">
                            <div>Yana Berman • <span data-timeago="1600453620">21:27, 18 September 2020</span></div>
                        </div>
                    </article>
                </div>
                <div>
                    <article class="newsCell" style="position: relative">
                        <a href="https://currency.com/ru/caixabank-i-bankia-sozdadut-samyj-bolshoj-bank-ispanii"
                           class="newsCell__link">
                            <div class="imageWrap">
                                <div class="imageCell">
                                    <img
                                        src="https://img.currency.com/imgs/articles/704xx/shutterstock_627570737.jpg"
                                        alt="CaixaBank и Bankia создадут самый большой банк Испании"
                                        title="CaixaBank и Bankia создадут самый большой банк Испании" class="image"
                                        loading="lazy">
                                </div>
                            </div>
                            <div class="newsCell__rubric">Новости рынков</div>
                            <div class="newsCell__title medium">CaixaBank и Bankia создадут самый большой банк
                                Испании
                            </div>
                            <div class="details">
                                <div>Ramla Soni • <span data-timeago="1600453200">21:20, 18 September 2020</span>
                                </div>
                            </div>
                        </a>
                    </article>


                    <article class="newsCell groupContainer">
                        <div class="newsCell__rubric">Аналитика</div>
                        <a href="https://currency.com/ru/analiz-kursa-rublya-21-27-sentyabrya"
                           class="newsCell__link">
                            <header class="newsCell__title medium">Анализ курса рубля: прогноз на неделю с 21 по 27
                                сентября
                            </header>
                        </a>
                        <div class="details">
                            <div>Стелла Киракосян • <span data-timeago="1600450680">20:38, 18 September 2020</span>
                            </div>
                        </div>
                    </article>


                    <article class="newsCell groupContainer">
                        <div class="newsCell__rubric">Обзоры рынков</div>
                        <a href="https://currency.com/ru/prognoz-kursa-bitkoina-chto-meshayet-rostu"
                           class="newsCell__link">
                            <header class="newsCell__title medium">Прогноз курса биткоина: что мешает росту и когда
                                цена перешагнет $20 тыс.
                            </header>
                        </a>
                        <div class="details">
                            <div>Tasha Makey • <span data-timeago="1600435260">16:21, 18 September 2020</span></div>
                        </div>
                    </article>


                    <article class="newsCell groupContainer">
                        <div class="newsCell__rubric">Новости рынков</div>
                        <a href="https://currency.com/ru/bytedance-planiruet-provesti-ipo-tiktok-v-ssha"
                           class="newsCell__link">
                            <header class="newsCell__title medium">ByteDance планирует провести IPO TikTok в США
                            </header>
                        </a>
                        <div class="details">
                            <div>Yana Berman • <span data-timeago="1600427040">14:04, 18 September 2020</span></div>
                        </div>
                    </article>


                    <article class="newsCell groupContainer">
                        <div class="newsCell__rubric">Новости рынков</div>
                        <a href="https://currency.com/ru/kriptobirzha-kraken-pervoj-v-istorii-poluchila-bankovskuyu-licenziyu-v-ssha"
                           class="newsCell__link">
                            <header class="newsCell__title medium">Криптобиржа Kraken первой в истории получила
                                банковскую лицензию в США
                            </header>
                        </a>
                        <div class="details">
                            <div>Lawrence Gash • <span data-timeago="1600419060">11:51, 18 September 2020</span>
                            </div>
                        </div>
                    </article>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="promo">
    <div class="apps">
        <div class="row__container">
            <div class="apps__inner">
                <div class="phone1Wrap">
                    <!--<img src="https://static.currency.com/img/media/phone1.c9b18a.png" alt="iPhone Image" class="phone1">-->
                </div>
                <div class="phone2Wrap">
                    <img src="https://static.currency.com/img/media/phone3.png" alt="iPhone Image" class="phone2">
                </div>
                <div class="apps__text desk">
                    Торгуйте мировыми токенизированными акциями, индексами, товарами и Форекс-парами с криптой и фиатом
                </div>
                <div class="imacWrap desk">
                    <img src="https://static.currency.com/img/media/imacc@2x.png" alt="iMac Image" class="imac">
                </div>
            </div>
            <div class="apps__footer">
                <div class="appsIconsWrap">
                    <div class="appsIcon">
                        <svg stroke="none" fill="none" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 56 56" id="logo-icon"><path d="M14.6894 3.35999L18.1014 11.805C12.6556 15.1407 9.02223 21.146 9.02223 28C9.02223 38.4811 17.5189 46.9777 28 46.9777C29.4184 46.9777 30.8004 46.8221 32.13 46.5271L35.5422 54.9725C33.1426 55.642 30.6131 56 28 56C12.536 56 0 43.4639 0 28C0 17.3539 5.94145 8.09562 14.6894 3.35999Z" fill="var(--color-logo-left, currentColor)"></path><path fill-rule="evenodd" clip-rule="evenodd" d="M28.0005 37.9556C22.5022 37.9556 18.0449 33.4983 18.0449 28C18.0449 22.5017 22.5022 18.0445 28.0005 18.0445C31.8749 18.0445 35.2325 20.2577 36.8777 23.4889H46.4389C44.4144 15.1855 36.9272 9.02223 28.0005 9.02223C26.5821 9.02223 25.2 9.17783 23.8705 9.4729L20.4583 1.02752C22.8579 0.357959 25.3874 0 28.0005 0C43.4645 0 56.0005 12.536 56.0005 28C56.0005 38.646 50.059 47.9044 41.3111 52.64L37.8991 44.195C42.1098 41.6158 45.237 37.4406 46.4389 32.5111H36.8777C36.2728 33.6992 35.4363 34.7497 34.4278 35.6031C32.6935 37.0707 30.4503 37.9556 28.0005 37.9556Z" fill="var(--color-logo-right, currentColor)"></path></svg>
                    </div>
                    <svg fill="currentColor" stroke="none" stroke-width="0" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 118 16" id="logo" class="apps__logo"><path d="M46 8.28516C46.01 8.11586 46.01 7.94612 46 7.77682C46.0114 5.33111 44.1594 3.27577 41.7186 3.02528C39.2778 2.77479 37.0445 4.41087 36.5557 6.80758C36.0668 9.20429 37.4819 11.5797 39.8271 12.299C42.1723 13.0183 44.6824 11.8469 45.63 9.5909H43.11C42.6357 10.0646 41.9914 10.3301 41.32 10.3285C40.1199 10.3332 39.0788 9.5031 38.82 8.33499H46V8.28516ZM41.32 5.22513C42.3071 5.22576 43.2059 5.79197 43.63 6.68039H39C39.4257 5.78872 40.3293 5.2219 41.32 5.22513ZM55.92 7.79675L55.92 12.5114H53.73V7.79675C53.7327 7.12 53.4655 6.46992 52.9873 5.98952C52.509 5.50911 51.859 5.23774 51.18 5.2351C50.7127 5.23552 50.2467 5.28564 49.79 5.38461V12.5114H47.6V3.48082C48.7607 3.13784 49.9705 2.98963 51.18 3.04225C53.805 3.05324 55.9255 5.18029 55.92 7.79675ZM33.88 3.04225H35V5.25504H33.88C33.1966 5.25767 32.5425 5.53146 32.0621 6.01589C31.5817 6.50032 31.3147 7.1555 31.32 7.83662V12.4715H29.13V7.83662C29.1193 6.5722 29.6145 5.35564 30.5058 4.45594C31.3972 3.55623 32.6114 3.04751 33.88 3.04225ZM26.57 3.04225H27.74V5.25504H26.57C25.8849 5.25502 25.2282 5.52764 24.7456 6.01236C24.2631 6.49708 23.9947 7.15378 24 7.83662V12.4715H21.83V7.83662C21.8194 6.57392 22.3131 5.35886 23.2023 4.45945C24.0915 3.56003 25.3032 3.05015 26.57 3.04225ZM17 3.04225H19.2V12.0728C18.0393 12.4158 16.8295 12.564 15.62 12.5114C14.3576 12.5061 13.1489 12.0012 12.26 11.1077C11.3711 10.2141 10.8747 9.00524 10.88 7.74691L10.88 3.04225H13.07V7.74691C13.0673 8.42366 13.3345 9.07375 13.8127 9.55415C14.2909 10.0346 14.941 10.3059 15.62 10.3086C16.0834 10.3059 16.5455 10.2592 17 10.169V3.04225ZM9.49 6.68039H7.15C6.71184 5.79058 5.80444 5.22627 4.81 5.22513C3.56666 5.23772 2.51218 6.13899 2.31005 7.36187C2.10792 8.58475 2.81659 9.77557 3.99016 10.1851C5.16373 10.5946 6.4628 10.1043 7.07 9.02275H9.45C8.78844 11.3063 6.55112 12.7644 4.18715 12.4527C1.82317 12.141 0.0431557 10.1532 0 7.77682C0.0449694 5.372 1.86715 3.3709 4.26418 3.09396C6.66121 2.81702 8.89433 4.34958 9.49 6.68039ZM97.32 3.04225C95.3979 3.03822 93.6628 4.1892 92.9245 5.95806C92.1861 7.72692 92.59 9.76497 93.9477 11.1211C95.3054 12.4773 97.3492 12.8842 99.1254 12.1519C100.902 11.4197 102.06 9.69268 102.06 7.77682C102.06 5.16588 99.9395 3.04775 97.32 3.04225ZM97.32 10.3285C96.2837 10.3325 95.3471 9.7134 94.9478 8.76023C94.5484 7.80706 94.7649 6.7079 95.4963 5.97606C96.2276 5.24421 97.3295 5.02407 98.2874 5.41843C99.2452 5.81279 99.87 6.74385 99.87 7.77682C99.87 9.18219 98.7299 10.323 97.32 10.3285ZM118 7.91636V12.631H115.81V7.91636C115.81 6.50549 114.665 5.3602 113.25 5.35471C112.58 5.35471 111.85 5.80325 111.85 6.27172V12.5114H109.66V7.79675C109.66 6.38587 108.515 5.24059 107.1 5.2351C106.633 5.23604 106.167 5.28616 105.71 5.38461V12.5114H103.52V3.48082C104.681 3.13784 105.891 2.98963 107.1 3.04225C108.322 3.123 109.477 3.63131 110.36 4.47757C111.085 3.64046 112.14 3.16005 113.25 3.16186C115.877 3.17284 118 5.29829 118 7.91636ZM79.87 7.84659C79.87 8.49066 79.3462 9.01279 78.7 9.01279C78.0538 9.01279 77.53 8.49066 77.53 7.84659C77.53 7.20251 78.0538 6.68039 78.7 6.68039C79.3462 6.68039 79.87 7.20251 79.87 7.84659ZM77.58 3.04225L72 16H69.5L71.21 12.013L67.55 3.04225H70.19L72.46 9.21214L75.08 3.04225H77.58ZM59.58 7.77682C59.5798 8.96055 60.3844 9.99395 61.5349 10.2877C62.6853 10.5814 63.8892 10.0608 64.46 9.02275H66.84C66.1921 11.3217 63.9486 12.7975 61.5732 12.4872C59.1977 12.1768 57.4115 10.1745 57.38 7.78678C57.425 5.38196 59.2472 3.38087 61.6442 3.10393C64.0412 2.82699 66.2743 4.35955 66.87 6.69036H64.53C63.9988 5.61992 62.8026 5.04985 61.6333 5.30991C60.4639 5.56997 59.6243 6.59283 59.6 7.78678L59.58 7.77682ZM84 7.77682C84.0146 8.94183 84.8104 9.95269 85.9421 10.244C87.0739 10.5354 88.2612 10.035 88.84 9.02275H91.22C90.5592 11.3048 88.3246 12.763 85.9619 12.454C83.5993 12.145 81.8178 10.1615 81.77 7.78678C81.815 5.38196 83.6371 3.38087 86.0342 3.10393C88.4312 2.82699 90.6643 4.35955 91.26 6.69036H88.92C88.4818 5.80054 87.5744 5.23624 86.58 5.2351C85.1667 5.23501 84.0164 6.36824 84 7.77682Z"></path></svg>
                </div>
                <div class="apps__btns">
                    <a target="_blank" rel="nofollow noopener" href="https://app.appsflyer.com/id1458917114" class="store-apple badge-appstore ios"></a>
                    <a target="_blank" rel="nofollow noopener" href="https://app.appsflyer.com/com.currency.exchange.prod2?" class="store-google badge-googleplay android"></a>
                    <a href="https://currency.com/trading/platform" target="_blank" class="badge-platform"></a>
{{--                    <a href="/ru#" class="qrBtn">--}}
{{--                        <svg xmlns="http://www.w3.org/2000/svg" width="28" height="28" fill="none" class="qrIcon"><path d="M2 2h7V0H0v9h2V2zm17-2v2h7v7h2V0h-9zm7 26h-7v2h9v-9h-2v7zM2 19H0v9h9v-2H2v-7z" fill="#fff"></path><path d="M6 6v7H3v2h3v7h16v-7h2v-2h-2V6H6zm2 2h12v5H8V8zm12 12H8v-5h12v5z" fill="#BBBDBF"></path></svg>--}}
{{--                    </a>--}}
                </div>
            </div>

        </div>
    </div>
</section>

<div class="row__container">

    <hr>

    <section class="best-offer">
        <div class="cr-widget" data-currency="USD" data-type="all" data-roi="both" data-sort="ROI" style="width: 100%; height: 750px;"><a target="_blank" rel="noopener" href="https://cryptorank.io/ieo-platforms-roi"></a></div><script src="https://cryptorank.io/widget/ieo-roi.js"></script>
    </section>
</div>

@endsection
