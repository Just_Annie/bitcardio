@extends('layouts.app')

@section('content')
    <div class="row__container exchanges">
        <div class="news-section-title">
            <h1>Биржи</h1>
            <br>
            <div class="cr-widget" data-currency="USD" data-type="all" data-roi="both" data-sort="ROI" style="width: 100%; height: 750px;"><a target="_blank" rel="noopener" href="https://cryptorank.io/ieo-platforms-roi"></a></div><script src="https://cryptorank.io/widget/ieo-roi.js"></script>
        </div>
    </div>
@endsection
