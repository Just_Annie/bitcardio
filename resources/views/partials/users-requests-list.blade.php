@if(!empty($list))
    <div class="col list-wrapper">
        <div class="card">
            <div class="card-header">
                {{ __('layout.contact-form') }}
            </div>
            <div class="card-body">
                <hr>
                <div class="form-group row">
                    <div class="col-1">
                        <h4>ID</h4>
                    </div>
                    <div class="col-1">
                        <h4>Имя</h4>
                    </div>
                    <div class="col-2">
                        <h4>E-Mail</h4>
                    </div>
                    <div class="col-2">
                        <h4>Тема</h4>
                    </div>
                    <div class="col message-container">
                        <h4>Сообщение</h4>
                    </div>
                    <div class="col-1">
                        <h4>Файл</h4>
                    </div>
                    <div class="col green">
                        <h4>Статус</h4>
                    </div>
                </div>
                <hr>
                @foreach($list as $item)
                    <?php /** @var \App\Models\UserRequest $item */ ?>
                    <div class="form-group row">
                        <div class="col-1">
                            {{ $item->id }}
                        </div>
                        <div class="col-1">
                            {{ $item->user->name }}
                        </div>
                        <div class="col-2">
                            {{ $item->user->email }}
                        </div>
                        <div class="col-2">
                            {{ $item->subject }}
                        </div>
                        <div class="col message-container">
                            {{ $item->message }}
                        </div>
                        <div class="col-1">
                            @if($item->file_path)
                                <a href="{{ $item->file_path }}" target="_blank">
                                    Файл
                                </a>
                            @endif
                        </div>
                        @if($item->is_answered)
                            <div class="col green">
                                {{ __('layout.answered') }}
                            </div>
                        @else
                            <div class="col">
                                <form method="POST" action="{{ route('client.mark.as.read', ['id' => $item->id]) }}">
                                    @csrf
                                    <button type="submit" class="btn btn-outline-success">
                                        {{ __('layout.mark-as-answered') }}
                                    </button>
                                </form>
                            </div>
                        @endif
                    </div>
                    <hr>
                @endforeach
                {{ $list->links() }}
            </div>
        </div>
    </div>
@endif
