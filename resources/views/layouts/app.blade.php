<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="https://allfont.ru/allfont.css?fonts=youand039re-gone" rel="stylesheet" type="text/css" />
</head>
<body @if(in_array(Route::getCurrentRoute()->getName(), ['login', 'register', 'client.home'])) class="dark-body"  @endif>
    <div id="app">
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark-custom">
            <div class="container">
                <a class="navbar-brand logo-brand" href="{{ url('/') }}">
                    {{ config('app.name', 'test-project') }}
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarNavDropdown">
                    <ul class="navbar-nav">
                        @auth
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('client.home') }}">Личный кабинет</a>
                            </li>
                        @endauth
                        <li class="nav-item dropdown">
                            <a class="nav-link" href="{{ route('news') }}">
                                Новости
                            </a>
                            <div class="dropdown-menu">
                                <a class="dropdown-item" href="{{ route('interview') }}">Интервью</a>
                                <a class="dropdown-item" href="{{ route('opinions') }}">Мнения</a>
                                <a class="dropdown-item" href="{{ route('today') }}">Сегодня</a>
                            </div>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link" href="{{ route('banking') }}">
                                Банкинг
                            </a>
                            <div class="dropdown-menu">
                                <a class="dropdown-item" href="{{ route('insurance') }}">Страхование</a>
                                <a class="dropdown-item" href="{{ route('credits') }}">Кредиты</a>
                                <a class="dropdown-item" href="{{ route('deposits') }}">Вклады</a>
                                <a class="dropdown-item" href="{{ route('cards') }}">Карты</a>
                            </div>
                        </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link" href="{{ route('monitoring') }}">
                                    Мониторинг
                                </a>
                                <div class="dropdown-menu">
                                    <a class="dropdown-item" href="{{ route('blockchain') }}">Блокчейн</a>
                                    <a class="dropdown-item" href="{{ route('mining') }}">Майнинг</a>
                                    <a class="dropdown-item" href="{{ route('trading') }}">Трейдинг</a>
                                    <a class="dropdown-item" href="{{ route('statistics') }}">Статистика</a>
                                </div>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link" href="{{ route('exchanges') }}">
                                    Биржи
                                </a>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link" href="{{ route('services') }}">
                                    Сервисы
                                </a>
                                <div class="dropdown-menu">
                                    <a class="dropdown-item" href="{{ route('aggregators') }}">Агрегаторы крипто-новостей</a>
                                </div>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link" href="{{ route('contacts') }}">
                                    Контакты
                                </a>
                            </li>
                    </ul>
                </div>
                <ul class="navbar-nav mr-sm-2">
                    @guest
                        <li class="nav-item">
                            <a class="btn btn-outline-success mr-3" href="{{ route('login') }}">Логин</a>
                        </li>
                        @if (Route::has('register'))
                            <li class="nav-item">
                                <a class="btn btn-outline-success" href="{{ route('register') }}">Регистрация</a>
                            </li>
                        @endif
                    @else
                        <li class="nav-item dropdown">
                            <a id="navbarDropdown" class="btn btn-outline-success dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>

                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    Выйти
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </div>
                        </li>
                    @endguest
                </ul>
            </div>
        </nav>
        @if(Route::getCurrentRoute()->getName() === 'welcome')
            <iframe style="width:100%;border:0;overflow:hidden;background-color:transparent;height:109px" scrolling="no" src="https://fortrader.org/informers/getInformer?st=31&cat=15&title=%D0%9A%D1%80%D0%B8%D0%BF%D1%82%D0%BE%D0%B2%D0%B0%D0%BB%D1%8E%D1%82%D1%8B&texts=%7B%22toolTitle%22%3A%22%D0%9A%D1%80%D0%B8%D0%BF%D1%82%D0%BE%D0%B2%D0%B0%D0%BB%D1%8E%D1%82%D0%B0%22%2C%22bid%22%3A%22%D0%A6%D0%B5%D0%BD%D0%B0%22%7D&mult=0.89&showGetBtn=0&w=0&hideDiff=1&colors=titleTextColor%3Dfff%2CtitleBackgroundColor%3D1b1b1b%2CsymbolTextColor%3Deeeeee%2CtableTextColor%3Df7f7f7%2CprofitTextColor%3D89bb50%2CprofitBackgroundColor%3Deaf7e1%2ClossTextColor%3Dff1616%2ClossBackgroundColor%3Df6e1e1%2CborderTdColor%3Df7f7f7%2CtableBorderColor%3D1b1b1b%2CtrBackgroundColor%3D1b1b1b%2CinformerLinkTextColor%3Df7f7f7%2CinformerLinkBackgroundColor%3Df6f6f6&items=133%2C25457%2C25470%2C25467%2C25469%2C25468%2C25496%2C25492%2C25499&columns="></iframe>
        @endif
        <main class="py-4">
            @yield('content')
        </main>
    </div>
    <footer>
        <div class="footer">
            <div>
                Cooperation by Anna Khamenko
            </div>
        </div>
    </footer>
</body>
</html>
