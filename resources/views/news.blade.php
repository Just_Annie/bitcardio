@extends('layouts.app')

@section('content')
    <div class="row__container news">
        <div class="news-section-title">
            <h1>Новости</h1>
            <br>
            <iframe style="width:100%;border:0;overflow:hidden;background-color:transparent;height:372px" scrolling="no" src="https://fortrader.org/informers/getInformer?st=51&cat=23&texts=%7B%22toolTitle%22%3A%22%D0%91%D0%B8%D1%80%D0%B6%D0%B5%D0%B2%D1%8B%D0%B5%20%D0%BD%D0%BE%D0%B2%D0%BE%D1%81%D1%82%D0%B8%22%7D&mult=1&w=0&slts=0&colors=titleTextColor%3Dc0c2cb%2CtitleBackgroundColor%3D232425%2CborderLeftColor%3Df8fafc%2CnewsBackgroundColor%3Df8fafc%2CnewsTextColor%3D232425%2CtimeTextColor%3D888888%2CnewsBorderColor%3Df8fafc&items=5%2C641%2C4343%2C11752"></iframe>
        </div>
    </div>
@endsection
