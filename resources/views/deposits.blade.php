@extends('layouts.app')

@section('content')
    <div class="row__container deposits">
        <div class="news-section-title">
            <h1>Вклады</h1>
            <br>
            <iframe style="width:100%;border:0;overflow:hidden;background-color:transparent;height:1045px" scrolling="no" src="https://fortrader.org/informers/getInformer?st=1&cat=16&title=%D0%98%D0%BD%D0%B4%D0%B5%D0%BA%D1%81%D1%8B&texts=%7B%22toolTitle%22%3A%22%D0%98%D0%BD%D0%B4%D0%B5%D0%BA%D1%81%22%2C%22bid%22%3A%22%D0%A6%D0%B5%D0%BD%D0%B0%22%2C%22ask%22%3A%22%D0%A1%D0%BF%D1%80%D0%BE%D1%81%22%2C%22openQuote%22%3A%22%D0%9E%D1%82%D0%BA%D1%80.%22%2C%22highQuote%22%3A%22%D0%9C%D0%B0%D0%BA%D1%81.%22%2C%22lowQuote%22%3A%22%D0%9C%D0%B8%D0%BD.%22%2C%22chg%22%3A%22%D0%98%D0%B7%D0%BC.%22%2C%22chgPer%22%3A%22%D0%98%D0%B7%D0%BC.%20%25%22%2C%22time%22%3A%22%D0%92%D1%80%D0%B5%D0%BC%D1%8F%22%7D&mult=1&showGetBtn=0&w=0&colors=false&items=25482%2C136%2C25477%2C25476%2C25480%2C137%2C25485%2C25488%2C25486%2C25479%2C25478%2C25483%2C25481%2C25484%2C25491%2C25489%2C25531%2C25534&columns=bid%2Cask%2CopenQuote%2ChighQuote%2ClowQuote%2Cchg%2CchgPer%2Ctime"></iframe>
        </div>
    </div>
@endsection
