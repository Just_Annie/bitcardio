<?php
/** @var \App\structure\UserRequestNotificationStructure $params */
?>
<!DOCTYPE html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>

        <title>{{ $subject }}</title>
    </head>
    <body>
        <p>User id - {{ $params->get(\App\structure\UserRequestNotificationStructure::F_ID) }}</p>
        <p>Subject - {{ $params->get(\App\structure\UserRequestNotificationStructure::F_SUBJECT) }}</p>
        <p>Message - {{ $params->get(\App\structure\UserRequestNotificationStructure::F_MESSAGE) }}</p>
        <p>User name - {{ $params->get(\App\structure\UserRequestNotificationStructure::F_NAME) }}</p>
        <p>User email - {{ $params->get(\App\structure\UserRequestNotificationStructure::F_EMAIL) }}</p>
        @if($params->get(\App\structure\UserRequestNotificationStructure::F_LINK))
            <p><a href="{{ $params->get(\App\structure\UserRequestNotificationStructure::F_LINK) }}">file</a></p>
        @endif
        <p>Create date - {{ $params->get(\App\structure\UserRequestNotificationStructure::F_CREATED_AT) }}</p>
    </body>
</html>