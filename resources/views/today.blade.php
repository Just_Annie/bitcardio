@extends('layouts.app')

@section('content')
    <div class="row__container today">
        <div class="news-section-title">
            <h1>Сегодня</h1>
            <br>
            <table cellpadding="0" cellspacing="10" bgcolor="#FBFBFB" width="100%" style="border-collapse:collapse;
                border-color:#D7D7D7;">
                <tr>
                    <td>
                        <a href="http://www.profinance.ru" id="forexpf_forex"
                style="font-size: 13px; margin-bottom: 5px; font-weight:bold">Валютный рынок Forex</a><br>
                        <script charset="utf-8"
                src="https://informers.forexpf.ru/export/news.js">
                        </script>
                    </td>
                </tr>
            </table>
            <table cellpadding="0" cellspacing="10" bgcolor="#FBFBFB" width="100%" style="border-collapse:collapse;
                    border-color:#D7D7D7;">
                <tr>
                    <td>
                        <a href="http://www.profinance.ru" id="forexpf_fond" style="font-size: 13px;
                    margin-bottom: 5px; font-weight:bold">Фондовый рынок</a><br>
                        <script
                    src="https://informers.forexpf.ru/export/fond.js">
                        </script>
                    </td>
                </tr>
            </table>
            <table cellpadding="0" cellspacing="10" bgcolor="#FBFBFB" width="100%" style="border-collapse:collapse;border-color:#D7D7D7;">
                <tr>
                    <td>
                        <a href="http://www.profinance.ru" id="forexpf_economic" style="font-size: 13px; margin-bottom: 5px; font-weight:bold">Новости экономики</a><br>
                        <script src="https://informers.forexpf.ru/export/economic.js">
                        </script>
                    </td>
                </tr>
            </table>
            <table cellpadding="0" cellspacing="10" bgcolor="#FBFBFB" width="100%" style="border-collapse:collapse;border-color:#D7D7D7;">
                <tr>
                    <td>
                        <a href="http://www.profinance.ru/chart/gold/" id="goldnewspf" style="font-size: 13px; margin-bottom: 5px; font-weight:bold">Золото</a><br>
                        <script src="//www.profinance.ru/_informer_/get_goldnews.php?col=5">
                        </script>
                    </td>
                </tr>
            </table>
        </div>
    </div>
@endsection
