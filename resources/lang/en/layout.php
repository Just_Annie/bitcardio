<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'send' => 'Send',
    'subject' => 'Subject',
    'message' => 'Message',
    'add-file' => 'Add file',
    'contact-form' => 'Contact Form',
    'today-already-send' => 'Today you already sent a request',
    'send-request-error' => 'An error has occurred, try again.',
    'send-request-resolution-error' => 'Attempt to resubmit request',
    'answered' => 'answered',
    'mark-as-answered' => 'mark as answered',
    'not-enough-permission' => 'Not enough permissions',
];
