<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'send' => 'Отправить',
    'subject' => 'Тема',
    'message' => 'Сообщение',
    'add-file' => 'Добавить файл',
    'contact-form' => 'Контактная форма',
    'today-already-send' => 'Сегодня вы уже отправили заявку',
    'send-request-error' => 'Произошла ошибка, попробуйте еще раз.',
    'send-request-resolution-error' => 'Попытка повторно отправить запрос',
    'answered' => 'Обработано',
    'mark-as-answered' => 'Отметить как обработано',
    'not-enough-permission' => 'Недостаточно разрешений',
];
